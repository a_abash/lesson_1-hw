/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/



window.onload = ( function () {
  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  var one, two, three;

  one = getRandomIntInclusive(0, 256);
  two = getRandomIntInclusive(0, 256);
  three = getRandomIntInclusive(0, 256);

  //function randomColor(a, b, c) {
  //  return 'rgb(' + a + ', ' + b + ', ' + c +')';
  //}
  function randomColor(aa, bb, cc) {
    return '#' + aa.toString(16) + bb.toString(16) + cc.toString(16);
  }

  var mine = document.getElementById('app');
      mine.style.background = randomColor(one, two, three);

  var div = document.createElement('div');
      div.innerText = randomColor(one, two, three);

  var btn = document.createElement('button');
      btn.innerText = 'Press me!';
      btn.setAttribute('onclick', 'document.location.reload()');

  var app = document.getElementById('app');

      app.style.width = '100%';
      app.style.height = '100vh';
      app.style.display = 'flex';
      app.style.justifyContent = 'center';
      app.style.alignItems = 'center';
      app.style.flexDirection = 'column';
      app.style.fontSize = '11vw';
      app.style.color = '#fff';
      app.style.fontWeight = 'bold';
      app.style.fontFamily= 'arial';
      btn.style.color = '#fff';
      btn.style.fontSize = '1.5vw';
      btn.style.padding = '5px 15px';
      btn.style.border = '2px solid #fff';
      btn.style.borderRadius = '25px';
      btn.style.background = randomColor(one, two, three  );
      app.appendChild(div);
      app.appendChild(btn);
});